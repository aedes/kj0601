package cn.kj0601.util;
/**
 * 统一返回格式  
 * @author Thinkpad
 * 
 */
public class ResultJson {
	/*
	 * 返回的状态 
	 * 200:成功
	 * 201:参数异常
	 * 203:逻辑异常-（没查，查了结果有问题）
	 * 205:数据异常-（查询失败） 
	 * 
	 */
	private Integer code;
	private String msg; //消息
	private Object data; //返回的数据
	
	
	
	/**
	 * @param code
	 * @param msg
	 * @param data
	 */
	private ResultJson(Integer code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}


	public static ResultJson ok() {
		return new ResultJson(200,"操作成功",null);
	}
	
	public static ResultJson ok(Object data) {
		return new ResultJson(200,"操作成功",data);
	}
	
	public static ResultJson ok(String msg,Object data) {
		return new ResultJson(200,msg,data);
	}
	
	public static ResultJson err(int code,String msg) {
		return new ResultJson(code,msg,null);
	}
	
	
	public Integer getCode() {
		return code;
	}
	public String getMsg() {
		return msg;
	}
	public Object getData() {
		return data;
	}

}
