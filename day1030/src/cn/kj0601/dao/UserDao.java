package cn.kj0601.dao;

import java.util.List;

import cn.kj0601.entity.UserDO;

public interface UserDao {
	List<UserDO> selectList(UserDO user);
}
