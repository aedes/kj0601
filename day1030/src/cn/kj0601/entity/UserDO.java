package cn.kj0601.entity;

public class UserDO {
	private Long id;
	private String username;
	private String pwd;
	/**
	 * 
	 */
	public UserDO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param id
	 * @param username
	 * @param pwd
	 */
	public UserDO(Long id, String username, String pwd) {
		this.id = id;
		this.username = username;
		this.pwd = pwd;
	}
	public Long getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public String getPwd() {
		return pwd;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	@Override
	public String toString() {
		return "UserDO [id=" + id + ", username=" + username + ", pwd=" + pwd + "]";
	}
	
	
}
