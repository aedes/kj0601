package cn.kj0601.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.kj0601.entity.UserDO;
import cn.kj0601.service.UserSerivce;
import cn.kj0601.util.ResultJson;

@Controller
@RequestMapping("user")
public class UserController {
	
	@Autowired
	UserSerivce userSerivce;
	
	@RequestMapping("test")
	@ResponseBody
	public Object test() {
		return userSerivce.test(null);
	}
	
	/**
	 * 使用对象接收参数-只要参数内容和对象匹配，会自动打包成对象
	 * @param user
	 * @return
	 */
	@RequestMapping("login")
	@ResponseBody
	public ResultJson login(UserDO user) {
		if(user==null) {
			return ResultJson.err(201, "参数未传递");
		}
		if(user.getUsername()==null||"".equals(user.getUsername())) {
			return ResultJson.err(201, "用户名不能为空");
		}
		if(user.getPwd()==null||"".equals(user.getPwd())) {
			return ResultJson.err(201, "密码不能为空");
		}
		
		
		return userSerivce.login(user);
	}
	
}
