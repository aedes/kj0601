package cn.kj0601.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.kj0601.dao.UserDao;
import cn.kj0601.entity.UserDO;
import cn.kj0601.util.ResultJson;

@Service
public class UserServiceImpl implements UserSerivce {
	private static Logger log = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	UserDao userDao;

	@Override
	public ResultJson login(UserDO user) {
		
		UserDO userPar = new UserDO();
		userPar.setUsername(user.getUsername());
		List<UserDO> list = userDao.selectList(userPar);
		if(list==null||list.size()==0) {
			log.info("登录失败,用户不存在");
			return ResultJson.err(203, "登录失败,用户不存在");
		}
		if(!list.get(0).getPwd().equals(user.getPwd())) {
			log.info("登录失败,用户名或密码错误");
			return ResultJson.err(203, "登录失败,用户名或密码错误");
		}
		
		
		return ResultJson.ok("登录成功", list.get(0));
	}
	
	/**
	 * 测试用
	 */
	@Override
	public List<UserDO> test(UserDO user) {
		// TODO Auto-generated method stub
		return userDao.selectList(user);
	}

}
